from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from Pente.tools import Pente
from django.views.decorators.http import require_http_methods
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions

@api_view(['PUT'])
@permission_classes((permissions.AllowAny,))
def board(request):
    """
    Get current board and return the next move for the requested user
    """
    pente = Pente(request.data)
    return Response(pente.get_data(), status=status.HTTP_200_OK)