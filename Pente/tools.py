import random

class Pente:
	def __init__(self, options):
		self.options = options
		self.note = {
			"empty" : 0,
			"line" : 7,
			"neightbor" : 9,
			"war" : 12,
			"obliterate" : 1,
			"win" : 2000,
			"bite" : 300,
		}
		self.oponnent = {
			1:2,
			2:1
		}

	def get_data(self):
		return self.start_ia()
		#result = self.start_ia()
		#return result

	def start_ia(self):
		max = -10000
		step = 2
		max_x = -1
		max_y = -1
		for x in range(0,19):
			for y in range(0,19):
				if(self.options["board"][x][y] == 0):
					if(self.options["round"] == 3):
						if(x >= 7 and y <= 11):
							continue
						if(y >= 7 and x <= 11):
							continue
					self.options["board"][x][y] = self.options["player"]
					tmp = self.eval(x,y)
					tmp += self.ia_min(step-1)
					if(x >= 5 and y <= 13 and y >= 5 and x <= 13):
						tmp += 20
					if(tmp == max):
						rand = random.randint(1,100)
						if(rand < 20):
							max = tmp
							max_x = x
							max_y = y
					if(tmp > max):
						max = tmp
						max_x = x
						max_y = y
					self.options["board"][x][y] = 0

		return {'x':max_x,'y':max_y}

	def ia_min(self, step):
		min = 10000
		for x in range(0,19):
			for y in range(0,19):
				if(self.options["board"][x][y] == 0):
					self.options["board"][x][y] = self.oponnent[self.options["player"]]
					tmp = -self.eval(x,y)
					if(step > 1):
						tmp += self.ia_max(step-1)
					if(tmp < min):
					    min = tmp
					self.options["board"][x][y] = 0
		return min

	def ia_max(self, step):
		max = -10000
		for x in range(0,19):
			for y in range(0,19):
				if(self.options["board"][x][y] == 0):
					self.options["board"][x][y] = self.options["player"]
					tmp = self.eval(x,y)
					if(x >= 6 and y <= 12):
						tmp += 2
					if(y >= 6 and x <= 12):
						tmp += 2
					if(step > 1):
						tmp += self.ia_min(step-1)
					if(tmp > max):
						max = tmp
					self.options["board"][x][y] = 0
		return max

	def eval(self, x, y):
		player = self.options["board"][x][y]
		result = 0
		##TOP - LEFT
		neightbor = True
		note = 0
		countNeightbor = 0
		index = -1
		if(x > 0 and y > 0):
			while(self.options["board"][x+index][y+index] != self.oponnent[player]):
				if(self.options["board"][x+index][y+index] == player):
					note += self.note["line"]+index*2
					if(neightbor):
						countNeightbor += 1
						if(countNeightbor<=5):
							note += self.note["neightbor"]+self.note["line"]*countNeightbor
						else:
							note -= self.note["neightbor"]*2
				else:
					neightbor = False
					note += self.note["empty"]
				index -= 1
				if(x+index < 0 or y+index < 0):
					break
				if(-index > 5):
					break
			if(-index <= 4):
				note += self.note["obliterate"]+index
			if(note == 0):
				index = -1
				countOpponent = 0
				while(self.options["board"][x+index][y+index] == self.oponnent[player]):
					countOpponent += 1
					note += self.note["war"]
					index -= 1
					if(x+index < 0 or y+index < 0):
						break
					if(-index > 4):
						break
				if(countOpponent == 2):
					note += self.note["bite"]
				if(countOpponent > 2):
					note += self.note["war"]*countOpponent
			result += note

		##BOTTOM - RIGHT
		neightbor = True
		note = 0
		index = 1
		if(x < 18 and y < 18):
			while(self.options["board"][x+index][y+index] != self.oponnent[player]):
				if(self.options["board"][x+index][y+index] == player):
					note += self.note["line"]-index*2
					if(neightbor):
						countNeightbor += 1
						if(countNeightbor<=5):
							note += self.note["neightbor"]+self.note["line"]*countNeightbor
						else:
							note -= self.note["neightbor"]*2
				else:
					neightbor = False
					note += self.note["empty"]
				index += 1
				if(x+index > 18 or y+index > 18):
					break
				if(index > 5):
					break
			if(index <= 4):
				note += self.note["obliterate"]-index
			if(note == 0):
				index = 1
				countOpponent = 0
				while(self.options["board"][x+index][y+index] == self.oponnent[player]):
					countOpponent += 1
					note += self.note["war"]
					index += 1
					if(x+index > 18 or y+index > 18):
						break
					if(index > 4):
						break
				if(countOpponent == 2):
					note += self.note["bite"]
				if(countOpponent > 2):
					note += self.note["war"]*countOpponent
			result += note
		if(countNeightbor == 4):
			result += self.note["win"]


		##TOP - RIGHT
		neightbor = True
		note = 0
		countNeightbor = 0
		index = -1
		if(x < 18 and y > 0):
			while(self.options["board"][x-index][y+index] != self.oponnent[player]):
				if(self.options["board"][x-index][y+index] == player):
					note += self.note["line"]+index*2
					if(neightbor):
						countNeightbor += 1
						if(countNeightbor<=5):
							note += self.note["neightbor"]+self.note["line"]*countNeightbor
						else:
							note -= self.note["neightbor"]*2
				else:
					neightbor = False
					note += self.note["empty"]
				index -= 1
				if(x-index > 18 or y+index < 0):
					break
				if(-index > 5):
					break
			if(index <= 4):
				note += self.note["obliterate"]+index
			if(note == 0):
				index = -1
				countOpponent = 0
				while(self.options["board"][x-index][y+index] == self.oponnent[player]):
					countOpponent += 1
					note += self.note["war"]
					index -= 1
					if(x-index > 18 or y+index < 0):
						break
					if(-index > 4):
						break
				if(countOpponent == 2):
					note += self.note["bite"]
				if(countOpponent > 2):
					note += self.note["war"]*countOpponent
			result += note

		##BOTTOM - LEFT
		neightbor = True
		note = 0
		index = 1
		if(x > 0 and y < 18):
			while(self.options["board"][x-index][y+index] != self.oponnent[player]):
				if(self.options["board"][x-index][y+index] == player):
					note += self.note["line"]-index*2
					if(neightbor):
						countNeightbor += 1
						if(countNeightbor<=5):
							note += self.note["neightbor"]+self.note["line"]*countNeightbor
						else:
							note -= self.note["neightbor"]*2
				else:
					neightbor = False
					note += self.note["empty"]
				index += 1
				if(x-index < 0 or y+index > 18):
					break
				if(index > 5):
					break
			if(index <= 4):
				note += self.note["obliterate"]-index
			if(note == 0):
				index = 1
				countOpponent = 0
				while(self.options["board"][x-index][y+index] == self.oponnent[player]):
					countOpponent += 1
					note += self.note["war"]
					index += 1
					if(x-index < 0 or y+index > 18):
						break
					if(index > 4):
						break
				if(countOpponent == 2):
					note += self.note["bite"]
				if(countOpponent > 2):
					note += self.note["war"]*countOpponent
			result += note
		if(countNeightbor == 4):
			result += self.note["win"]


		##TOP
		neightbor = True
		note = 0
		countNeightbor = 0
		index = -1
		if(y > 0):
			while(self.options["board"][x][y+index] != self.oponnent[player]):
				if(self.options["board"][x][y+index] == player):
					note += self.note["line"]+index*2
					if(neightbor):
						countNeightbor += 1
						if(countNeightbor<=5):
							note += self.note["neightbor"]+self.note["line"]*countNeightbor
						else:
							note -= self.note["neightbor"]*2
				else:
					neightbor = False
					note += self.note["empty"]
				index -= 1
				if(y+index < 0):
					break
				if(-index > 5):
					break
			if(index <= 4):
				note += self.note["obliterate"]+index
			if(note == 0):
				index = -1
				countOpponent = 0
				while(self.options["board"][x][y+index] == self.oponnent[player]):
					countOpponent += 1
					note += self.note["war"]
					index -= 1
					if(y+index < 0):
						break
					if(-index > 4):
						break
				if(countOpponent == 2):
					note += self.note["bite"]
				if(countOpponent > 2):
					note += self.note["war"]*countOpponent
			result += note

		##BOTTOM
		neightbor = True
		note = 0
		index = 1
		if(y < 18):
			while(self.options["board"][x][y+index] != self.oponnent[player]):
				if(self.options["board"][x][y+index] == player):
					note += self.note["line"]-index*2
					if(neightbor):
						countNeightbor += 1
						if(countNeightbor<=5):
							note += self.note["neightbor"]+self.note["line"]*countNeightbor
						else:
							note -= self.note["neightbor"]*2
				else:
					neightbor = False
					note += self.note["empty"]
				index += 1
				if(y+index > 18):
					break
				if(index > 5):
					break
			if(index <= 4):
				note += self.note["obliterate"]-index
			if(note == 0):
				index = 1
				countOpponent = 0
				while(self.options["board"][x][y+index] == self.oponnent[player]):
					countOpponent += 1
					note += self.note["war"]
					index += 1
					if(y+index > 18):
						break
					if(index > 4):
						break
				if(countOpponent == 2):
					note += self.note["bite"]
				if(countOpponent > 2):
					note += self.note["war"]*countOpponent
			result += note
		if(countNeightbor == 4):
			result += self.note["win"]


		##LEFT
		neightbor = True
		note = 0
		countNeightbor = 0
		index = -1
		if(x > 0):
			while(self.options["board"][x+index][y] != self.oponnent[player]):
				if(self.options["board"][x+index][y] == player):
					note += self.note["line"]+index*2
					if(neightbor):
						countNeightbor += 1
						if(countNeightbor<=5):
							note += self.note["neightbor"]+self.note["line"]*countNeightbor
						else:
							note -= self.note["neightbor"]*2
				else:
					neightbor = False
					note += self.note["empty"]
				index -= 1
				if(x+index < 0):
					break
				if(-index > 5):
					break
			if(index <= 4):
				note += self.note["obliterate"]+index
			if(note == 0):
				index = -1
				countOpponent = 0
				while(self.options["board"][x+index][y] == self.oponnent[player]):
					countOpponent += 1
					note += self.note["war"]
					index -= 1
					if(x+index < 0):
						break
					if(-index > 4):
						break
				if(countOpponent == 2):
					note += self.note["bite"]
				if(countOpponent > 2):
					note += self.note["war"]*countOpponent
			result += note

		##RIGHT
		neightbor = True
		note = 0
		index = 1
		if(x < 18):
			while(self.options["board"][x+index][y] != self.oponnent[player]):
				if(self.options["board"][x+index][y] == player):
					note += self.note["line"]-index*2
					if(neightbor):
						countNeightbor += 1
						if(countNeightbor<=5):
							note += self.note["neightbor"]+self.note["line"]*countNeightbor
						else:
							note -= self.note["neightbor"]*2
				else:
					neightbor = False
					note += self.note["empty"]
				index += 1
				if(x+index > 18):
					break
				if(index > 5):
					break
			if(index <= 4):
				note += self.note["obliterate"]-index
			if(note == 0):
				index = 1
				countOpponent = 0
				while(self.options["board"][x+index][y] == self.oponnent[player]):
					countOpponent += 1
					note += self.note["war"]
					index += 1
					if(x+index > 18):
						break
					if(index > 4):
						break
				if(countOpponent == 2):
					note += self.note["bite"]
				if(countOpponent > 2):
					note += self.note["war"]*countOpponent
			result += note
		if(countNeightbor == 4):
			result += self.note["win"]

		return result

