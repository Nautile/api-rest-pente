from django.conf.urls import url, include
from django.contrib.auth.models import User
from Pente import views

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^board/$', views.board)
]